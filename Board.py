import random


class Board:
    random_numbers = list()
    # Make class attribute so reduce check_win() execution time (no need to create matrix each iteration)
    board_win = [[1, 5, 9, 13], [2, 6, 10, 14], [3, 7, 11, 15], [4, 8, 12, 0]]

    # A set is a hash map... int is used by hashing... So using a list and pop random position to a new list
    """def init_numbers(self):
        numbers = list()
        for i in range(1, 16):
            numbers.append(i)
        for i in range(15):
            random_number = random.randint(0, 14 - i)
            self.random_numbers.append(numbers[random_number])
            numbers.pop(random_number)"""

    """def __init__(self):
        self.board = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
        self.init_numbers()
        self.empty_x = -1
        self.empty_y = -1
        random_number = random.random()
        allocate_empty = False
        for i in range(4):
            for j in range(4):
                if i == 3 and j == 3 and not allocate_empty:
                    break
                if random_number > 0.85 and not allocate_empty:
                    allocate_empty = True
                    self.empty_x = i
                    self.empty_y = j
                else:
                    self.board[i][j] = self.random_numbers.pop()
                    random_number = random.random()
        self.mov_score = 0"""

    def __init__(self):
        self.board = [[1, 5, 9, 13], [2, 6, 10, 14], [3, 7, 11, 15], [4, 8, 12, 0]]
        self.empty_x = 3
        self.empty_y = 3
        self.mov_score = 0
        random_number = random.random()
        for i in range(380):
            if random_number < 0.25:
                self.update_board('up')
            elif random_number < 0.5:
                self.update_board('down')
            elif random_number < 0.75:
                self.update_board('left')
            else:
                self.update_board('right')
            random_number = random.random()

    def print_board(self):
        board_numbers = ""
        for i in range(4):
            for j in range(4):
                board_numbers = board_numbers + str(self.board[i][j]) + " "
            board_numbers += '\n'
        return board_numbers

    def check_win(self):
        return self.board.__eq__(self.board_win)

    def get_item(self, x, y):
        return self.board[x][y]

    # Make empty_x, empty_y class attributes, so no necessary iterate each time
    # O(1) vs O(N)
    def get_empty_position(self):
        return self.empty_x, self.empty_y

    def __len__(self):
        return len(self.board)

    def update_board(self, direction):
        if direction == 'left':
            if self.empty_x != 3:
                self.board[self.empty_x][self.empty_y] = self.board[self.empty_x + 1][self.empty_y]
                self.board[self.empty_x + 1][self.empty_y] = 0
                self.empty_x += 1
        elif direction == 'right':
            if self.empty_x != 0:
                self.board[self.empty_x][self.empty_y] = self.board[self.empty_x - 1][self.empty_y]
                self.board[self.empty_x - 1][self.empty_y] = 0
                self.empty_x -= 1
        elif direction == 'up':
            if self.empty_y != 3:
                self.board[self.empty_x][self.empty_y] = self.board[self.empty_x][self.empty_y + 1]
                self.board[self.empty_x][self.empty_y + 1] = 0
                self.empty_y += 1
        elif direction == 'down':
            if self.empty_y != 0:
                self.board[self.empty_x][self.empty_y] = self.board[self.empty_x][self.empty_y - 1]
                self.board[self.empty_x][self.empty_y - 1] = 0
                self.empty_y -= 1

    def increment_mov_score(self):
        self.mov_score += 1

    def get_mov_score(self):
        return self.mov_score

    def set_board(self, other_board, score):
        self.board = other_board
        for i in range(4):
            for j in range(4):
                if other_board[i][j] == 0:
                    self.empty_x = i
                    self.empty_y = j
                    break
        self.mov_score = int(score)
