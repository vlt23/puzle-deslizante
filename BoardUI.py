import os
import sys
import traceback

import pygame
from pygame.locals import *

import Board

DEBUG = False

N = 4
WIDTH = 640
HEIGHT = 480
TILE_SIZE = 80
POINT_SIZE = 60

# RGB
BLUE = (0, 64, 255)
ORANGE = (255, 128, 0)
WHITE = (255, 255, 255)
DARKGREEN = (8, 80, 32)

MARGIN_X = int((WIDTH - (TILE_SIZE * N + (N - 1))) / 2)
MARGIN_Y = int((HEIGHT - (TILE_SIZE * N + (N - 1))) / 2)


def main():
    pygame.init()
    global font, display_surf, new_surf, new_rect, save_surf, save_rect, fps_lock, load_surf, load_rect

    font = pygame.font.Font('LiberationSerif-Regular.ttf', 20)
    display_surf = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption('Puzle deslizante')
    fps_lock = pygame.time.Clock()

    new_surf, new_rect = button_text('Empezar juego', WHITE, ORANGE, WIDTH - 120, HEIGHT - 120)
    save_surf, save_rect = button_text('Guardar juego', WHITE, ORANGE, WIDTH - 120, HEIGHT - 90)
    load_surf, load_rect = button_text('Cargar juego', WHITE, ORANGE, WIDTH - 120, HEIGHT - 60)

    main_board = generate_new_puzzle()

    while True:
        slide_to = None
        clicked_x = -1
        clicked_y = -1
        draw_board(main_board)

        for event in pygame.event.get():
            # User clicked X, exit game
            if event.type == QUIT:
                sys.exit(0)
            # Mouse click
            elif event.type == MOUSEBUTTONUP:
                clicked_x, clicked_y = get_clicked(main_board, event.pos[0], event.pos[1])
                # Button click
                if (clicked_x, clicked_y) == (None, None):
                    # New game button click
                    if new_rect.collidepoint(event.pos):
                        main_board = generate_new_puzzle()
                    # Save game button click
                    elif save_rect.collidepoint(event.pos):
                        try:
                            file = open('saved_game', 'w')
                            file.write(main_board.print_board())
                            file.write(str(main_board.get_mov_score()))
                            file.close()
                        except IOError:
                            print(traceback.format_exc())
                    # Load game button click
                    elif load_rect.collidepoint(event.pos):
                        aux_board, score = load_game()
                        main_board.set_board(aux_board, score)
                # Check if win or no inside the for event and before tile click to mute all slide
                # Player only can click on new game or load game
                elif main_board.check_win():
                    if DEBUG:
                        print('You WIN!')
                # Tile click
                else:
                    empty_x, empty_y = main_board.get_empty_position()
                    if clicked_x == empty_x + 1 and clicked_y == empty_y:
                        slide_to = 'left'
                    elif clicked_x == empty_x - 1 and clicked_y == empty_y:
                        slide_to = 'right'
                    elif clicked_x == empty_x and clicked_y == empty_y + 1:
                        slide_to = 'up'
                    elif clicked_x == empty_x and clicked_y == empty_y - 1:
                        slide_to = 'down'

        if slide_to:
            slide_animation(main_board, slide_to, clicked_x, clicked_y)
            main_board.update_board(slide_to)
            main_board.increment_mov_score()

        pygame.display.update()
        fps_lock.tick(30)


def button_text(text, text_color, title_color, top, left):
    text_surf = font.render(text, True, text_color, title_color)
    text_rect = text_surf.get_rect()
    text_rect.topleft = (top, left)
    return text_surf, text_rect


def draw_board(board):
    display_surf.fill(DARKGREEN)
    for tile_x in range(len(board)):
        for tile_y in range(len(board)):
            if board.get_item(tile_x, tile_y) != 0:
                draw_tile(tile_x, tile_y, board.get_item(tile_x, tile_y))

    left, top = get_left_top_of_tile(0, 0)
    width = N * TILE_SIZE
    height = N * TILE_SIZE
    pygame.draw.rect(display_surf, BLUE, (left - 5, top - 5, width + 11, height + 11), N)

    display_surf.blit(new_surf, new_rect)
    display_surf.blit(save_surf, save_rect)
    display_surf.blit(load_surf, load_rect)

    draw_score(board.get_mov_score())


def generate_new_puzzle():
    board = Board.Board()
    draw_board(board)
    pygame.display.update()
    pygame.time.wait(500)
    return board


def draw_tile(tile_x, tile_y, num, mov_x=0, mov_y=0):
    left, top = get_left_top_of_tile(tile_x, tile_y)
    if DEBUG:
        print(left, top)
    pygame.draw.rect(display_surf, ORANGE, (left + mov_x, top + mov_y, TILE_SIZE, TILE_SIZE))
    text_surf = font.render(str(num), True, WHITE)
    text_rect = text_surf.get_rect()
    text_rect.center = left + int(TILE_SIZE / 2) + mov_x, top + int(TILE_SIZE / 2) + mov_y
    display_surf.blit(text_surf, text_rect)


def get_left_top_of_tile(tile_x, tile_y):
    left = MARGIN_X + tile_x * TILE_SIZE + tile_x - 1
    top = MARGIN_Y + tile_y * TILE_SIZE + tile_y - 1
    return left, top


def get_clicked(board, x, y):
    for tile_x in range(len(board)):
        for tile_y in range(len(board)):
            left, top = get_left_top_of_tile(tile_x, tile_y)
            tile_rect = pygame.Rect(left, top, TILE_SIZE, TILE_SIZE)
            if tile_rect.collidepoint(x, y):
                return tile_x, tile_y
    return None, None


def slide_animation(board, direction, move_x, move_y):
    base_surf = display_surf.copy()
    move_left, move_top = get_left_top_of_tile(move_x, move_y)
    pygame.draw.rect(base_surf, DARKGREEN, (move_left, move_top, TILE_SIZE, TILE_SIZE))

    for i in range(0, TILE_SIZE, 8):
        display_surf.blit(base_surf, (0, 0))
        if direction == 'left':
            draw_tile(move_x, move_y, board.get_item(move_x, move_y), -i, 0)
        elif direction == 'right':
            draw_tile(move_x, move_y, board.get_item(move_x, move_y), i, 0)
        elif direction == 'up':
            draw_tile(move_x, move_y, board.get_item(move_x, move_y), 0, -i)
        elif direction == 'down':
            draw_tile(move_x, move_y, board.get_item(move_x, move_y), 0, i)

        pygame.display.update()
        fps_lock.tick(30)


def draw_score(score):
    pygame.draw.rect(display_surf, ORANGE, (WIDTH - 100, HEIGHT - 360, POINT_SIZE, POINT_SIZE))
    text_surf = font.render(str(score), True, WHITE)
    text_rect = text_surf.get_rect()
    text_rect.center = WIDTH - 100 + int(POINT_SIZE / 2), HEIGHT - 360 + int(POINT_SIZE / 2)
    display_surf.blit(text_surf, text_rect)


def load_game():
    aux_board = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
    try:
        if os.path.exists('saved_game'):
            file = open('saved_game', 'r')
            for i in range(4):
                nums = file.readline()
                j = 0
                w = 0
                while nums[w] != '\n':
                    if nums[w].isdigit():
                        if nums[w + 1].isdigit():
                            aux_board[i][j] = 10 + int(nums[w + 1])
                            w = w + 3
                            j += 1
                        else:
                            aux_board[i][j] = int(nums[w])
                            w = w + 2
                            j += 1
            score = file.readline()
            file.close()
            return aux_board, score
    except IOError:
        print(traceback.format_exc())
